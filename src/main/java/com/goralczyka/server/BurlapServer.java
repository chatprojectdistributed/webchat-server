package com.goralczyka.server;

import com.goralczyka.chat.services.ChatService;
import com.goralczyka.chat.services.WebChatService;
import lombok.extern.log4j.Log4j;
import org.springframework.context.annotation.Bean;
import org.springframework.remoting.caucho.BurlapServiceExporter;
import org.springframework.remoting.support.RemoteExporter;

/**
 * Created by adam on 14.05.17.
 */
@Log4j
public class BurlapServer {

    @Bean(name = "/burlap")
    RemoteExporter bookingService() {
        BurlapServiceExporter exporter = new BurlapServiceExporter();
        exporter.setService(new WebChatService());
        exporter.setServiceInterface(ChatService.class);
        return exporter;
    }
}
