package com.goralczyka.server;

import com.goralczyka.chat.services.ChatService;
import com.goralczyka.chat.services.WebChatService;
import lombok.extern.log4j.Log4j;
import org.springframework.context.annotation.Bean;
import org.springframework.remoting.caucho.HessianServiceExporter;
import org.springframework.remoting.support.RemoteExporter;

/**
 * Created by adam on 14.05.17.
 */
@Log4j
public class HessianServer {

    @Bean(name = "/hessian")
    RemoteExporter bookingService() {
        HessianServiceExporter exporter = new HessianServiceExporter();
        exporter.setService(new WebChatService());
        exporter.setServiceInterface(ChatService.class);
        return exporter;
    }
}
