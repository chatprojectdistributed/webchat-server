package com.goralczyka.webchat;

import com.goralczyka.chat.exceptions.UsernameAlreadyExistsException;
import com.goralczyka.server.BurlapServer;
import com.goralczyka.server.HessianServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@ComponentScan("com.goralczyka.chat")
@Import({HessianServer.class, BurlapServer.class})
public class WebchatApplication {

	public static void main(String[] args) throws UsernameAlreadyExistsException {
		SpringApplication.run(WebchatApplication.class, args);
	}
}
