package com.goralczyka.utils;

import static java.util.UUID.randomUUID;

/**
 * Created by adam on 14.05.17.
 */
public class TokenUtil {
    public static String generateToken() {
        return randomUUID().toString();
    }
}
