package com.goralczyka.chat.exceptions;

/**
 * Created by adam on 14.05.17.
 */
public class UsernameAlreadyExistsException extends Exception {
    public UsernameAlreadyExistsException(final String message) {
        super(message);
    }
}
