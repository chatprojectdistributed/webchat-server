package com.goralczyka.chat.services;

import com.goralczyka.chat.exceptions.UsernameAlreadyExistsException;
import com.goralczyka.chat.model.User;
import com.goralczyka.utils.TokenUtil;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Serwis odpowiedzialny za zarządzanie użytkownikami czatu.
 * Created by adam on 14.05.17.
 */
@Service
public class UserService {

    /**
     * Czas od ostatniego requestu po którym użytkownika można uznać za nieaktywnego.
     */
    private static final Long TIME_TO_INACTIVE = 1000L * 30;
    
    /**
     * Mapa aktywnych username'ów.
     * Klucz - username
     * Wartosc - token użytkownika
     */
    private final Map<String, String> activeUsernames = new HashMap<>();

    /**
     * Mapa aktywnych użytkowników.
     * Klucz - token
     * Wartosc - użytkownik
     */
    private final Map<String, User> activeUsers = new HashMap<>();

    /**
     * Metoda dodaje usera do listy aktywnych użytkowników
     * @param username nazwa użytkownika
     * @return token użytkownika
     */
    public String addUser(final String username) throws UsernameAlreadyExistsException {
        if (isActive(username)) {
            throw new UsernameAlreadyExistsException("Username already exists");
        }
        String userToken = TokenUtil.generateToken();
        activeUsers.put(userToken, new User(username));
        activeUsernames.put(username, userToken);
        return userToken;
    }

    /**
     * Metoda usuwa użytkownika jeśli jest aktywny.
     * @param userToken token użytkownika
     * @return czy użytkownik został usunięty
     */
    public Boolean removeUser(final String userToken) {
        if (activeUsernames.remove(activeUsers.remove(userToken).getUsername()) != null)
            return true;
        return false;
    }

    /**
     * Metoda zwraca zbiór tokenów aktywnych użytkowników.
     * @return tokeny aktywnych użytkowników.
     */
    public Set<String> getActiveUserTokens() {
        return activeUsers.keySet();
    }

    /**
     * Zwraca nazwę użytkownika na podstawie tokenu.
     * @param token token użytkownika
     * @return nazwa użytkownika
     */
    public String getUsername(final String token) {
        return activeUsers.get(token).getUsername();
    }

    /**
     * Metoda aktualizuje datę ostatniego requestu użytkownika.
     * @param userToken
     */
    public void updateDateOfLastRequest(final String userToken) {
        User user = activeUsers.get(userToken);
        if (user != null)
            user.setLastRequestDate(new Date());
    }

    /**
     * Czy użytkownik jest zalogowany.
     * @param username nazwa użytkownika
     * @return czy użytkownik jest zalogowany
     */
    private Boolean isActive(final String username) {
        return activeUsernames.containsKey(username);
    }

    /**
     * Zwraca informację czy user jest nieaktywny. O jego nieaktywności decyduje czas
     * @param userToken
     * @return
     */
    public Boolean isUserInactive(final String userToken) {
        Date now = new Date();
        return (now.getTime() - activeUsers.get(userToken).getLastRequestDate().getTime()) > TIME_TO_INACTIVE;
    }
}
