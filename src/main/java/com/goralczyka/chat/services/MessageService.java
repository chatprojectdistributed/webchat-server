package com.goralczyka.chat.services;

import com.google.gson.Gson;
import com.goralczyka.chat.model.Message;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Serwis odpowiedzialny za obsługę wiadomości czatu.
 * Created by adam on 14.05.17.
 */
@Service
public class MessageService {

    private UserService userService;

    /**
     * Storage nieodebranych wiadomości.
     * Klucz - token użytkownika
     * Wartość - lista wiadomości
     */
    private final Map<String, List<Message>> messageStorage = new HashMap<>();

    public MessageService(final UserService userService) {
        this.userService = userService;
    }

    /**
     * Metoda dodaje wiadomość do storage.
     * @param senderToken token użytkownika
     * @param text tekst wiadomości
     */
    public void addMessage(final String senderToken, final String text) {
        if (isAllowedToSendMsg(senderToken)) {
            Message message = new Message(userService.getUsername(senderToken), text);

            Set<String> activeUserTokens = userService.getActiveUserTokens();
            for (String token : activeUserTokens) {
                List<Message> messages = messageStorage.get(token);
                if (messages == null) {
                    messages = new ArrayList<>();
                    messages.add(message);
                    messageStorage.put(token, messages);
                }
                else {
                    messages.add(message);
                }
            }
            userService.updateDateOfLastRequest(senderToken);
        }
        // tu moze byc else throw exception
    }

    /**
     * Czy użytkownik może wysłać wiadomość
     * @param senderToken token użytkownika
     * @return flaga informująca o możliwości wysłania wiadomości przez użytkownika
     */
    private Boolean isAllowedToSendMsg(final String senderToken) {
        return userService.getActiveUserTokens().contains(senderToken);
    }

    /**
     * Metoda zwraca listę wiadomości nie odczytanych przez użytkownika i usuwa wiadomości ze storage.
     * @param userToken token użytkownika
     * @return json z listą wiadomości
     */
    public String getMessages(final String userToken) {
        userService.updateDateOfLastRequest(userToken);
        return parseMassageListToJson(messageStorage.remove(userToken));
    }

    /**
     * Metoda parsuje listę wiadomości do jsona.
     * @param messages lista wiadomości
     * @return json z listą wiadomości
     */
    private String parseMassageListToJson(final List<Message> messages) {
        if (messages == null)
            return new Gson().toJson(new ArrayList<Message>());
        return new Gson().toJson(messages);
    }

    /**
     * Usuwa wszystkie wiadomosci uzytkownika.
     * @param token token uzytkownika
     */
    public void removeMessageForUser(final String token) {
        messageStorage.remove(token);
    }

}
