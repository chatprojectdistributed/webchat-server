package com.goralczyka.chat.services;

import com.goralczyka.chat.exceptions.UsernameAlreadyExistsException;

/**
 * Created by adam on 14.05.17.
 */
public interface ChatService {
    String login(String username) throws UsernameAlreadyExistsException;
    Boolean send(String userToken, String message);
    String getMessages(String userToken);
    Boolean logout(String userToken);
}
