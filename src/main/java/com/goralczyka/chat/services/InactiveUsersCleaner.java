package com.goralczyka.chat.services;

import java.util.Iterator;
import java.util.Set;

/**
 * Created by adam on 17.05.17.
 */
public class InactiveUsersCleaner {

    private UserService userService;

    private MessageService messageService;

    public InactiveUsersCleaner(final UserService userService, final MessageService messageService) {
        this.userService = userService;
        this.messageService = messageService;
    }

    public void clean() {
        Set<String> userTokens = userService.getActiveUserTokens();
        System.out.println("USER TOKENS COUNT: " + userTokens.size());
        Iterator<String> iterator = userTokens.iterator();
        while (iterator.hasNext()) {
            String token = iterator.next();
            if (userService.isUserInactive(token))
                removeUserAndHisMessages(token);
        }
    }

    private void removeUserAndHisMessages(final String token) {
        System.out.println("REMOVING USER " + userService.getUsername(token));
        userService.removeUser(token);
        messageService.removeMessageForUser(token);
    }
}
