package com.goralczyka.chat.services;

import com.goralczyka.chat.exceptions.UsernameAlreadyExistsException;
import org.springframework.stereotype.Service;

/**
 * Created by adam on 14.05.17.
 */
@Service
public class WebChatService implements ChatService {

    private static UserService userService = new UserService();

    private static MessageService messageService = new MessageService(userService);

    private static InactiveUsersCleaner inactiveUsersCleaner = new InactiveUsersCleaner(userService, messageService);

    @Override
    public String login(final String username) throws UsernameAlreadyExistsException {
        return userService.addUser(username);
    }

    @Override
    public Boolean send(final String userToken, final String message) {
        inactiveUsersCleaner.clean();
        messageService.addMessage(userToken, message);
        return true;
    }

    @Override
    public String getMessages(final String userToken) {
        return messageService.getMessages(userToken);
    }

    @Override
    public Boolean logout(final String userToken) {
        return userService.removeUser(userToken);
    }
}
