package com.goralczyka.chat.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * Created by adam on 14.05.17.
 */
@Getter
public class User {
    private final String username;

    @Setter
    private Date lastRequestDate;

    public User(final String username) {
        this.username = username;
        this.lastRequestDate = new Date();
    }
}
