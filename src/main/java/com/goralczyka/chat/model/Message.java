package com.goralczyka.chat.model;

import lombok.Getter;

import java.util.Date;

/**
 * Created by adam on 14.05.17.
 */
@Getter
public class Message {

    private final String username;
    private final String text;
    private final Date date;

    public Message(String username, String text) {
        this.username = username;
        this.text = text;
        this.date = new Date();
    }
}
